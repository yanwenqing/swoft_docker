/*
Navicat MySQL Data Transfer

Source Server         : deepin
Source Server Version : 50630
Source Host           : 192.168.153.130:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50630
File Encoding         : 65001

Date: 2020-01-03 17:45:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for products_view
-- ----------------------------
DROP TABLE IF EXISTS `products_view`;
CREATE TABLE `products_view` (
  `view_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` int(11) DEFAULT NULL,
  `view_ip` varchar(30) DEFAULT NULL,
  `view_num` int(11) DEFAULT '0',
  `view_date` date DEFAULT NULL,
  PRIMARY KEY (`view_id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of products_view
-- ----------------------------
INSERT INTO `products_view` VALUES ('44', '2', '192.168.153.1', '7', '2020-01-03');

-- ----------------------------
-- Table structure for products_class
-- ----------------------------
DROP TABLE IF EXISTS `products_class`;
CREATE TABLE `products_class` (
  `pclass_id` int(11) NOT NULL AUTO_INCREMENT,
  `pclass_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`pclass_id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of products_class
-- ----------------------------
INSERT INTO `products_class` VALUES ('22', '技术开发类');
INSERT INTO `products_class` VALUES ('23', '研发类');
INSERT INTO `products_class` VALUES ('24', '人文类');
INSERT INTO `products_class` VALUES ('25', '职场');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `prod_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_name` varchar(255) DEFAULT NULL,
  `prod_price` decimal(10,2) DEFAULT NULL,
  `prod_stock` int(255) DEFAULT NULL,
  `prod_cid` int(11) DEFAULT NULL,
  `prod_click` int(11) DEFAULT '0',
  PRIMARY KEY (`prod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1', 'php从入门到精通', '20.00', '20', '22', '3');
INSERT INTO `products` VALUES ('2', 'Java从入门到跑路', '30.00', '5', '22', '14');
INSERT INTO `products` VALUES ('3', 'golang从精通到入门', '20.00', '12', '23', '0');
INSERT INTO `products` VALUES ('4', '商品4', '12.00', '5', '23', '0');
INSERT INTO `products` VALUES ('5', '商品5', '33.00', '12', '23', '0');
INSERT INTO `products` VALUES ('6', '商品6', '23.00', '11', '23', '0');
INSERT INTO `products` VALUES ('7', '商品7', '12.00', '6', '24', '0');
INSERT INTO `products` VALUES ('8', '商品8', '15.00', '67', '24', '0');
INSERT INTO `products` VALUES ('9', '商品9', '24.00', '19', '24', '0');
INSERT INTO `products` VALUES ('10', '商品10', '52.00', '98', '24', '0');
INSERT INTO `products` VALUES ('11', '商品11', '11.00', '33', '24', '0');
INSERT INTO `products` VALUES ('12', '商品12', '27.00', '42', '25', '0');
INSERT INTO `products` VALUES ('13', '商品13', '51.00', '45', '25', '0');
INSERT INTO `products` VALUES ('14', '商品14', '66.00', '32', '25', '0');
INSERT INTO `products` VALUES ('15', '商品15', '33.00', '22', '25', '0');

-- ----------------------------
-- Table structure for orders_main
-- ----------------------------
DROP TABLE IF EXISTS `orders_main`;
CREATE TABLE `orders_main` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单ID。自增主键',
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单号',
  `user_id` int(11) DEFAULT NULL,
  `order_status` tinyint(4) DEFAULT '1' COMMENT '0已取消 1未付款 2 已经付款 3 正在配送 4 交易完成 5交易关闭',
  `order_money` decimal(10,2) DEFAULT NULL COMMENT '订单金额',
  `create_time` datetime DEFAULT NULL COMMENT '订单创建时间',
  PRIMARY KEY (`order_id`),
  UNIQUE KEY `order_no` (`order_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of orders_main
-- ----------------------------

-- ----------------------------
-- Table structure for orders_detail
-- ----------------------------
DROP TABLE IF EXISTS `orders_detail`;
CREATE TABLE `orders_detail` (
  `order_did` int(11) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(50) DEFAULT NULL COMMENT '订单号',
  `prod_id` int(11) DEFAULT NULL COMMENT '商品ID',
  `prod_name` varchar(255) DEFAULT NULL COMMENT '商品名称',
  `prod_price` decimal(10,2) DEFAULT NULL COMMENT '商品价格',
  `discount` tinyint(4) DEFAULT NULL COMMENT '折扣，只能填1-10.代表1折2折...',
  `prod_num` int(11) DEFAULT NULL COMMENT '购买数量',
  `totalmoney` decimal(10,2) DEFAULT NULL COMMENT '小计',
  `prod_remark` varchar(2000) DEFAULT NULL COMMENT '商品备注',
  PRIMARY KEY (`order_did`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of orders_detail
-- ----------------------------
