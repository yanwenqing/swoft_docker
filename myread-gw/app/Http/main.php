<?php

namespace App\Http;

use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;
use  Swoft\Http\Server\Annotation\Mapping\RequestMethod;

/**
 * 网关主类
 * @package App\Http
 * @Controller()
 */
class main
{
    /**
     * @RequestMapping("/{service}/{method}",method={RequestMethod::POST,RequestMethod::OPTIONS})
     */
    public function test(string $service, string $method)
    {
        $params = jsonParams();//获取请求JSON参数，返回是一个数组
        $host = "tcp://127.0.0.1:18307";

        try {
            $result = requestRPC($host, "App\\Rpc\\Lib\\" . $service, $method, $params);
        } catch (\Exception $e) {
            $result = ["error" => $e->getMessage()];
        }

        return $result;
    }
}