<?php

function OrderCreateSuccess($orderNo){
    return ["status"=>"success","orderNo"=>$orderNo];
}
function OrderCreateFail($msg="error"){
    return ["status"=>$msg,"orderNo"=>""];
}

function genInWhere($array){
    $where="";
    foreach($array as $item ){
        if($where=="")
            $where.="'".$item."'";
        else{
            $where.=",'".$item."'";
        }
    }
    return $where;

}