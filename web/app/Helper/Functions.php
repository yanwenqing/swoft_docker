<?php

use Swoft\Db\DB;
use Swoft\Http\Server\Annotation\Mapping\RequestMethod;
require_once(__DIR__."/WebFunctions.php");
require_once(__DIR__."/OrdersFunctions.php");

function response($contentType=false){
    if($contentType){
        return Swoft\Context\Context::mustGet()->getResponse()->withContentType($contentType);
    }
    return Swoft\Context\Context::mustGet()->getResponse();
}
function request()  {
    return Swoft\Context\Context::mustGet()->getRequest();
}
function isGet(){
    return request()->getMethod()==RequestMethod::GET;
}
function isPost(){
    return request()->getMethod()==RequestMethod::POST;
}
function toArray(object $obj){
    return json_decode(json_encode($obj),1);
}
function ip(){
    $req=request();
    if($req->server('http_x_forwarded_for')){
        return $req->server('http_x_forwarded_for');
    }elseif($req->server('http_client_ip')){
        return $req->server('http_client_ip');
    }else{
        return $req->server('remote_addr');
    }
}


function tx(callable $func,&$result=null){
    DB::beginTransaction();
    try{
        $result=$func();
        DB::commit();
    }catch (Exception $exception){
        $result=OrderCreateFail($exception->getMessage());
        DB::rollBack();
    }
}



function NewProduct(int $pid,string $pname){
    $p=new stdClass();
    {
        $p->pid=$pid;
        $p->pname=$pname.$p->pid;
    }

    return $p;
}
