<?php declare(strict_types=1);


namespace App\Models;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;


/**
 *
 * Class OrdersMain
 *
 * @since 2.0
 *
 * @Entity(table="orders_main")
 */
class OrdersMain extends Model
{
    /**
     * 订单ID。自增主键
     * @Id()
     * @Column(name="order_id", prop="orderId")
     *
     * @var int
     */
    private $orderId;

    /**
     * 订单号
     *
     * @Column(name="order_no", prop="orderNo")
     *
     * @var string|null
     */
    private $orderNo;

    /**
     *
     *
     * @Column(name="user_id", prop="userId")
     *
     * @var int|null
     */
    private $userId;

    /**
     * 0已取消 1未付款 2 已经付款 3 正在配送 4 交易完成 5交易关闭
     *
     * @Column(name="order_status", prop="orderStatus")
     *
     * @var int|null
     */
    private $orderStatus;

    /**
     * 订单金额
     *
     * @Column(name="order_money", prop="orderMoney")
     *
     * @var float|null
     */
    private $orderMoney;

    /**
     * 订单创建时间
     *
     * @Column(name="create_time", prop="createTime")
     *
     * @var string|null
     */
    private $createTime;


    /**
     * @param int $orderId
     *
     * @return void
     */
    public function setOrderId(int $orderId): void
    {
        $this->orderId = $orderId;
    }

    /**
     * @param string|null $orderNo
     *
     * @return void
     */
    public function setOrderNo(?string $orderNo): void
    {
        $this->orderNo = $orderNo;
    }

    /**
     * @param int|null $userId
     *
     * @return void
     */
    public function setUserId(?int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @param int|null $orderStatus
     *
     * @return void
     */
    public function setOrderStatus(?int $orderStatus): void
    {
        $this->orderStatus = $orderStatus;
    }

    /**
     * @param float|null $orderMoney
     *
     * @return void
     */
    public function setOrderMoney(?float $orderMoney): void
    {
        $this->orderMoney = $orderMoney;
    }

    /**
     * @param string|null $createTime
     *
     * @return void
     */
    public function setCreateTime(?string $createTime): void
    {
        $this->createTime = $createTime;
    }

    /**
     * @return int
     */
    public function getOrderId(): ?int
    {
        return $this->orderId;
    }

    /**
     * @return string|null
     */
    public function getOrderNo(): ?string
    {
        return $this->orderNo;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @return int|null
     */
    public function getOrderStatus(): ?int
    {
        return $this->orderStatus;
    }

    /**
     * @return float|null
     */
    public function getOrderMoney(): ?float
    {
        return $this->orderMoney;
    }

    /**
     * @return string|null
     */
    public function getCreateTime(): ?string
    {
        return $this->createTime;
    }

    private $orderItems;

    /**
     * @return mixed
     */
    public function getOrderItems()
    {
        return $this->orderItems;
    }

    /**
     * @param mixed $orderItems
     */
    public function setOrderItems($orderItems): void
    {
        $this->orderItems = $orderItems;
    }

}
