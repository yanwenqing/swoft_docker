<?php declare(strict_types=1);


namespace App\Models;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;


/**
 * 
 * Class ProductsView
 *
 * @since 2.0
 *
 * @Entity(table="products_view")
 */
class ProductsView extends Model
{
    /**
     * 
     * @Id()
     * @Column(name="view_id", prop="viewId")
     *
     * @var int
     */
    private $viewId;

    /**
     * 
     *
     * @Column(name="prod_id", prop="prodId")
     *
     * @var int|null
     */
    private $prodId;

    /**
     * 
     *
     * @Column(name="view_ip", prop="viewIp")
     *
     * @var string|null
     */
    private $viewIp;

    /**
     * 
     *
     * @Column(name="view_num", prop="viewNum")
     *
     * @var int|null
     */
    private $viewNum;

    /**
     * 
     *
     * @Column(name="view_date", prop="viewDate")
     *
     * @var string|null
     */
    private $viewDate;


    /**
     * @param int $viewId
     *
     * @return void
     */
    public function setViewId(int $viewId): void
    {
        $this->viewId = $viewId;
    }

    /**
     * @param int|null $prodId
     *
     * @return void
     */
    public function setProdId(?int $prodId): void
    {
        $this->prodId = $prodId;
    }

    /**
     * @param string|null $viewIp
     *
     * @return void
     */
    public function setViewIp(?string $viewIp): void
    {
        $this->viewIp = $viewIp;
    }

    /**
     * @param int|null $viewNum
     *
     * @return void
     */
    public function setViewNum(?int $viewNum): void
    {
        $this->viewNum = $viewNum;
    }

    /**
     * @param string|null $viewDate
     *
     * @return void
     */
    public function setViewDate(?string $viewDate): void
    {
        $this->viewDate = $viewDate;
    }

    /**
     * @return int
     */
    public function getViewId(): ?int
    {
        return $this->viewId;
    }

    /**
     * @return int|null
     */
    public function getProdId(): ?int
    {
        return $this->prodId;
    }

    /**
     * @return string|null
     */
    public function getViewIp(): ?string
    {
        return $this->viewIp;
    }

    /**
     * @return int|null
     */
    public function getViewNum(): ?int
    {
        return $this->viewNum;
    }

    /**
     * @return string|null
     */
    public function getViewDate(): ?string
    {
        return $this->viewDate;
    }

}
