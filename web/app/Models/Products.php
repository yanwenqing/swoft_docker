<?php declare(strict_types=1);


namespace App\Models;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;


/**
 * 
 * Class Products
 *
 * @since 2.0
 *
 * @Entity(table="products")
 */
class Products extends Model
{
    /**
     * 
     * @Id()
     * @Column(name="prod_id", prop="prodId")
     *
     * @var int
     */
    private $prodId;

    /**
     * 
     *
     * @Column(name="prod_name", prop="prodName")
     *
     * @var string|null
     */
    private $prodName;

    /**
     * 
     *
     * @Column(name="prod_price", prop="prodPrice")
     *
     * @var float|null
     */
    private $prodPrice;

    /**
     * 
     *
     * @Column(name="prod_stock", prop="prodStock")
     *
     * @var int|null
     */
    private $prodStock;

    /**
     * 
     *
     * @Column(name="prod_cid", prop="prodCid")
     *
     * @var int|null
     */
    private $prodCid;

    /**
     * 
     *
     * @Column(name="prod_click", prop="prodClick")
     *
     * @var int|null
     */
    private $prodClick;


    /**
     * @param int $prodId
     *
     * @return void
     */
    public function setProdId(int $prodId): void
    {
        $this->prodId = $prodId;
    }

    /**
     * @param string|null $prodName
     *
     * @return void
     */
    public function setProdName(?string $prodName): void
    {
        $this->prodName = $prodName;
    }

    /**
     * @param float|null $prodPrice
     *
     * @return void
     */
    public function setProdPrice(?float $prodPrice): void
    {
        $this->prodPrice = $prodPrice;
    }

    /**
     * @param int|null $prodStock
     *
     * @return void
     */
    public function setProdStock(?int $prodStock): void
    {
        $this->prodStock = $prodStock;
    }

    /**
     * @param int|null $prodCid
     *
     * @return void
     */
    public function setProdCid(?int $prodCid): void
    {
        $this->prodCid = $prodCid;
    }

    /**
     * @param int|null $prodClick
     *
     * @return void
     */
    public function setProdClick(?int $prodClick): void
    {
        $this->prodClick = $prodClick;
    }

    /**
     * @return int
     */
    public function getProdId(): ?int
    {
        return $this->prodId;
    }

    /**
     * @return string|null
     */
    public function getProdName(): ?string
    {
        return $this->prodName;
    }

    /**
     * @return float|null
     */
    public function getProdPrice(): ?float
    {
        return $this->prodPrice;
    }

    /**
     * @return int|null
     */
    public function getProdStock(): ?int
    {
        return $this->prodStock;
    }

    /**
     * @return int|null
     */
    public function getProdCid(): ?int
    {
        return $this->prodCid;
    }

    /**
     * @return int|null
     */
    public function getProdClick(): ?int
    {
        return $this->prodClick;
    }

}
