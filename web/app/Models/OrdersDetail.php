<?php declare(strict_types=1);


namespace App\Models;

use Swoft\Db\Annotation\Mapping\Column;
use Swoft\Db\Annotation\Mapping\Entity;
use Swoft\Db\Annotation\Mapping\Id;
use Swoft\Db\Eloquent\Model;


/**
 * 
 * Class OrdersDetail
 *
 * @since 2.0
 *
 * @Entity(table="orders_detail")
 */
class OrdersDetail extends Model
{
    /**
     * 
     * @Id()
     * @Column(name="order_did", prop="orderDid")
     *
     * @var int
     */
    private $orderDid;

    /**
     * 订单号
     *
     * @Column(name="order_no", prop="orderNo")
     *
     * @var string|null
     */
    private $orderNo;

    /**
     * 商品ID
     *
     * @Column(name="prod_id", prop="prodId")
     *
     * @var int|null
     */
    private $prodId;

    /**
     * 商品名称
     *
     * @Column(name="prod_name", prop="prodName")
     *
     * @var string|null
     */
    private $prodName;

    /**
     * 商品价格
     *
     * @Column(name="prod_price", prop="prodPrice")
     *
     * @var float|null
     */
    private $prodPrice;

    /**
     * 折扣，只能填1-10.代表1折2折...
     *
     * @Column()
     *
     * @var int|null
     */
    private $discount;

    /**
     * 购买数量
     *
     * @Column(name="prod_num", prop="prodNum")
     *
     * @var int|null
     */
    private $prodNum;

    /**
     * 小计
     *
     * @Column()
     *
     * @var float|null
     */
    private $totalmoney;

    /**
     * 商品备注
     *
     * @Column(name="prod_remark", prop="prodRemark")
     *
     * @var string|null
     */
    private $prodRemark;


    /**
     * @param int $orderDid
     *
     * @return void
     */
    public function setOrderDid(int $orderDid): void
    {
        $this->orderDid = $orderDid;
    }

    /**
     * @param string|null $orderNo
     *
     * @return void
     */
    public function setOrderNo(?string $orderNo): void
    {
        $this->orderNo = $orderNo;
    }

    /**
     * @param int|null $prodId
     *
     * @return void
     */
    public function setProdId(?int $prodId): void
    {
        $this->prodId = $prodId;
    }

    /**
     * @param string|null $prodName
     *
     * @return void
     */
    public function setProdName(?string $prodName): void
    {
        $this->prodName = $prodName;
    }

    /**
     * @param float|null $prodPrice
     *
     * @return void
     */
    public function setProdPrice(?float $prodPrice): void
    {
        $this->prodPrice = $prodPrice;
    }

    /**
     * @param int|null $discount
     *
     * @return void
     */
    public function setDiscount(?int $discount): void
    {
        $this->discount = $discount;
    }

    /**
     * @param int|null $prodNum
     *
     * @return void
     */
    public function setProdNum(?int $prodNum): void
    {
        $this->prodNum = $prodNum;
    }

    /**
     * @param float|null $totalmoney
     *
     * @return void
     */
    public function setTotalmoney(?float $totalmoney): void
    {
        $this->totalmoney = $totalmoney;
    }

    /**
     * @param string|null $prodRemark
     *
     * @return void
     */
    public function setProdRemark(?string $prodRemark): void
    {
        $this->prodRemark = $prodRemark;
    }

    /**
     * @return int
     */
    public function getOrderDid(): ?int
    {
        return $this->orderDid;
    }

    /**
     * @return string|null
     */
    public function getOrderNo(): ?string
    {
        return $this->orderNo;
    }

    /**
     * @return int|null
     */
    public function getProdId(): ?int
    {
        return $this->prodId;
    }

    /**
     * @return string|null
     */
    public function getProdName(): ?string
    {
        return $this->prodName;
    }

    /**
     * @return float|null
     */
    public function getProdPrice(): ?float
    {
        return $this->prodPrice;
    }

    /**
     * @return int|null
     */
    public function getDiscount(): ?int
    {
        return $this->discount;
    }

    /**
     * @return int|null
     */
    public function getProdNum(): ?int
    {
        return $this->prodNum;
    }

    /**
     * @return float|null
     */
    public function getTotalmoney(): ?float
    {
        return $this->totalmoney;
    }

    /**
     * @return string|null
     */
    public function getProdRemark(): ?string
    {
        return $this->prodRemark;
    }

}
