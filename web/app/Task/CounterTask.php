<?php
namespace App\Tasks;
use Swoft\Db\DB;
use Swoft\Task\Annotation\Mapping\Task;
use Swoft\Task\Annotation\Mapping\TaskMapping;

/**
 * Class OrderTask
 * @Task(name="counter")
 */
class CounterTask{
    /**
     * @TaskMapping(name="logincout")
     */
    public function logincout(int $startMonth,int $endMonth){

        $ret_obj= DB::table("user_login")
            ->selectRaw("DATE_FORMAT(logintime,'%Y%m%d') as dd,count(*) as cc")
            ->where("logintime",">=","2019-0$startMonth-01")
            ->where("logintime","<","2019-0$endMonth-01")
            ->groupBy(DB::raw("DATE_FORMAT(logintime,'%Y%m%d') "));
        return $ret_obj->get()->toArray();
    }
}