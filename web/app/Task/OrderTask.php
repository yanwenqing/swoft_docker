<?php
namespace App\Tasks;
use Swoft\Redis\Redis;
use Swoft\Task\Annotation\Mapping\Task;
use Swoft\Task\Annotation\Mapping\TaskMapping;

/**
 * Class OrderTask
 * @Task(name="orders")
 */
class OrderTask{

    /**
     * @TaskMapping(name="putorderno")
     */
    public function putOrderNoToRedis($orderNo){

        Redis::setex("order".$orderNo,5,1);
        echo "插入redis成功".PHP_EOL;
    }
    /**
     * @TaskMapping(name="putorderno_zset")
     */
    public function putOrderNoToRedisZset($orderNo,$delay){

        Redis::zAdd("orderset",[$orderNo=>time()+$delay]);
        echo "插入redis_zset成功".PHP_EOL;
    }

}