<?php
namespace App\Console\Command;

use Swoft\Console\Annotation\Mapping\Command;
use Swoft\Console\Annotation\Mapping\CommandArgument;
use Swoft\Console\Annotation\Mapping\CommandMapping;
use Swoft\Console\Annotation\Mapping\CommandOption;
use Swoft\Console\Helper\Show;
use Swoft\Console\Input\Input;
use Swoft\Db\DB;

/**
 * Class CacheCmd
 * @Command(name="cache")
 */
class CacheCmd{

    /**
     *  @CommandMapping()
     * @CommandArgument("t", type="string",
     *     desc="类型有logincout等"
     * )
     * @CommandOption("start",short="s", type="integer",
     *     desc="起始时间，--start 或 -s"
     * )
     * @CommandOption("end",short="e", type="integer",
     *     desc="结束时间，--end 或 -e"
     * )
     * @example
     *   php {binFile} cache:run t=logincout --start=1 --end=3
     * @param Input $input
     */
    public function run(Input $input){
        $getType=$input->getArg("t","logincount");
        $start=intval($input->getOpt("start",1));
        $end=intval($input->getOpt("end",1))+1;
        $cache_data= DB::table("user_login")
            ->selectRaw("DATE_FORMAT(logintime,'%Y%m%d') as d,count(*) as c")
            ->where("logintime",">=","2019-0$start-01")
            ->where("logintime","<","2019-0$end-01")
            ->groupBy(DB::raw("DATE_FORMAT(logintime,'%Y%m%d') "))->get()->toArray();//数组


        foreach($cache_data as $data){
            DB::table("cache_logincount")
                ->updateOrInsert(['d'=>$data["d"]],$data);
        }


    }

}
