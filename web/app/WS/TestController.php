<?php
namespace App\WS;
use Swoft\Session\Session;
use Swoft\WebSocket\Server\Annotation\Mapping\MessageMapping;
use Swoft\WebSocket\Server\Annotation\Mapping\WsController;
use Swoft\WebSocket\Server\Message\Message;

/**
 * Class TestController
 * @package App\WS
 * @WsController(prefix="user")
 */
class TestController{

    /**
     * @MessageMapping(command="age")
     */
    public function age(Message $msg){
        var_dump($msg->getData());
        Session::mustGet()->push("this is msg from age");
    }
    /**
     * @MessageMapping(command="sex")
     */
    public function sex(Message $msg){
        var_dump($msg->getData());
        Session::mustGet()->push("this is msg from sex");
    }
}