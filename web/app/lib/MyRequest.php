<?php

namespace App\lib;
class MyRequest
{
    private $result;
    private $do = false;

    function if($bool)
    {
        $this->do = $bool;
        return clone $this;
    }

    function then(callable $func)
    {
        if ($this->do) {
            $this->result = $func();
            $this->do = !$this->do;
        }
        return clone $this;
    }

    function getResult()
    {
        return $this->result;
    }
}
