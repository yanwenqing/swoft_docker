<?php
namespace App\Http\Controller;
use App\Exception\ApiException;
use App\Models\OrdersDetail;
use App\Models\OrdersMain;
use Swoft\Db\DB;
use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;
use  Swoft\Http\Server\Annotation\Mapping\RequestMethod;
use Swoft\Redis\Redis;
use Swoft\Task\Task;
use Swoft\Validator\Annotation\Mapping\Validate;

/**
 * @Controller(prefix="/count")
 */
class Counter{

    /**
     * @RequestMapping(route="logincount",method={RequestMethod::GET})
     */
    public function logincount(){

//      $ret_obj= DB::table("user_login")
//          ->selectRaw("DATE_FORMAT(logintime,'%Y%m%d') as dd,count(*) as cc")
//          ->where("logintime",">=","2019-02-01")
//          ->where("logintime","<","2019-04-01")
//          ->groupBy(DB::raw("DATE_FORMAT(logintime,'%Y%m%d') "));
        $tasks = [
            [
                'counter',//task名称
                'logincout', //方法名
                [2,3]
            ],
            [
                'counter',
                'logincout',
                [3,4]
            ]
        ];
        $ret= Task::cos($tasks);

        return array_merge($ret[0],$ret[1]);
     //   return $ret_obj->get()->toArray();


    }

}