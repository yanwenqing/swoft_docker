<?php declare(strict_types=1);
/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://swoft.org/docs
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Http\Controller;

use App\Models\Products;
use App\Models\ProductsView;
use Swoft\Db\DB;
use Swoft\Http\Server\Annotation\Mapping\Controller;
use Swoft\Http\Server\Annotation\Mapping\RequestMapping;
use Swoft\Http\Server\Annotation\Mapping\RequestMethod;
use Swoft\Co;
use Swoft\Redis\Redis;

// use Swoft\Http\Message\Response;

/**
 * Class TestController
 *
 * @Controller(prefix="/test")
 * @package App\Http\Controller
 */
class TestController
{
    /**
     * Get data list. access uri path: /test
     * @RequestMapping(route="/test", method=RequestMethod::GET)
     */
    public function index()
    {
        return Redis::zRangeByScore("orderset",'-inf',strval(time()));
        //return ['item0', 'item1'];
    }

    /**
     * Get one by ID. access uri path: /test/{pid}
     * @RequestMapping(route="{pid}",params={"pid"="\d+"}, method=RequestMethod::GET)
     * @throws \Exception
     * @return array
     */
    public function get(int $pid): array
    {
//        // 1.字段复制和保存
//        {
//            $product_view = ProductsView::new();
//            $product_view->setProdId($pid);
//            $product_view->setViewIp(ip());
//            $product_view->setViewNum(1);
//            $product_view->setViewDate(date('Y-m-d'));
//            $product_view->save();
//        }
//        // 2.参数比较多的用法
//        {
//            $pview = [
//                "prod_id" => $pid,
//                "view_ip" => ip(),
//                "view_num" => 1,
//                "view_date" => date('Y-m-d')
//            ];
//            ProductsView::new($pview)->save();
//        }
        // 协程处理一些任务
        {
            Co::create(function () use ($pid) {
                // 普通更新
                Products::where('prod_id', $pid)->increment('prod_click');
            });
        }
        {
            Co::create(function () use ($pid) {
                // 创建或者更新
                ProductsView::updateOrCreate(
                    ["view_ip" => ip(), "view_date" => date('Y-m-d'), "prod_id" => $pid],
                    ["view_num" => DB::raw("view_num + 1")]
                );
            });
        }

        return ['item0'];
    }

    /**
     * Create a new record. access uri path: /test
     * @RequestMapping(route="/test", method=RequestMethod::POST)
     * @return array
     */
    public function post(): array
    {
        return ['id' => 2];
    }

    /**
     * Update one by ID. access uri path: /test/{id}
     * @RequestMapping(route="{id}", method=RequestMethod::PUT)
     * @return array
     */
    public function put(): array
    {
        return ['id' => 1];
    }

    /**
     * Delete one by ID. access uri path: /test/{id}
     * @RequestMapping(route="{id}", method=RequestMethod::DELETE)
     * @return array
     */
    public function del(): array
    {
        return ['id' => 1];
    }
}
