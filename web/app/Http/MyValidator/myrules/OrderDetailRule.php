<?php
namespace App\Http\MyValidator\myrules;

use Swoft\Bean\Annotation\Mapping\Bean;
use Swoft\Validator\Contract\RuleInterface;
use Swoft\Validator\Exception\ValidatorException;

/**
 * Class OrderDetailRule
 * @Bean(OrderDetail::class)
 */
class OrderDetailRule implements RuleInterface{

    /**
     * @param array $data
     * @param string $propertyName
     * @param object $item
     * @param mixed $default
     *
     * @return array
     */
    public function validate(array $data, string $propertyName, $item, $default = null): array
    {
        $getData=$data[$propertyName];
        if(!$getData || !is_array($getData) || count($getData)==0){
            throw new ValidatorException($item->getMessage());
        }
        foreach ($getData as $data){
            \validate($data,"orders_detail");
        }
        return $data;


    }
}