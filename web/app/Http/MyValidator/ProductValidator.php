<?php
namespace App\Http\MyValidator;

use Swoft\Validator\Annotation\Mapping\IsFloat;
use Swoft\Validator\Annotation\Mapping\IsString;
use Swoft\Validator\Annotation\Mapping\Length;
use Swoft\Validator\Annotation\Mapping\Max;
use Swoft\Validator\Annotation\Mapping\Min;
use Swoft\Validator\Annotation\Mapping\Validator;

/**
 * 商品验证
 * @Validator(name="product")
 */
class ProductValidator{
    /**
     * @IsString(message="商品名称不能为空")
     * @Length(min=5,max=20,message="商品名称字符长度为5--20")
     * @var string
     */
   protected $prod_name;
    /**
     * @IsString(message="商品短名称不能为空")
     * @Length(min=2,max=10,message="商品短名称字符长度为2--10")
     * @var string
     */
    protected $prod_sname;

    /**
     * @IsFloat(message="商品价格不能为空")
     * @Min(value=20,message="价格最低20块")
     * @Max(value=1000,message="价格最高1000块")
     * @var float
     */
   protected $prod_price;
}