<?php
namespace App\MyProcess;
use App\Models\OrdersMain;
use Swoft\Bean\Annotation\Mapping\Bean;
use Swoft\Process\Process;
use Swoft\Process\UserProcess;
use Swoft\Redis\Redis;
use Swoole\Coroutine;

/**
 * Class OrderProcess
 * @package App\MyProcess
 * @Bean()
 */
class OrderProcess extends UserProcess {

    /**
     * Run
     *
     * @param Process $process
     */
    public function run(Process $process): void
    {
//        while (true){
//            echo "test".PHP_EOL;
//            \Swoole\Coroutine::sleep(3);
//        }
        Redis::subscribe(["__keyevent@0__:expired"], function ($redis, $chan, $msg) {
             $orderNo=str_replace("order","",$msg);
             OrdersMain::where("order_no",$orderNo)
                 ->update(["order_status"=>-1]);
             echo "订单号为".$orderNo."的订单已经过期".PHP_EOL;
        });

    }
}
