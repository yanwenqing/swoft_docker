<?php
namespace App\MyProcess;
use Swoft\Bean\Annotation\Mapping\Bean;
use Swoft\Db\DB;
use Swoft\Process\Process;
use Swoft\Process\UserProcess;
use Swoft\Redis\Redis;

/**
 * Class OrderProcess
 * @package App\MyProcess
 * @Bean()
 */
class OrderProcessBySet extends  UserProcess{


    public function run(Process $process): void
    {
        while(true){
            $orders=Redis::zRangeByScore("orderset",'-inf',strval(time()),["limit"=>[0,3]]);

            if($orders && count($orders)>0)
            {
                DB::beginTransaction();
                try{
                    //update xxx set xxx where order_no in('xxx','xxx')
                    $rows=DB::update("update orders_main set order_status=-1 where order_no in("
                        .genInWhere($orders).")");
                    if(Redis::zRem("orderset",...$orders)!==$rows)
                        throw new \Exception("orderset rm error");
                    DB::commit();
                }catch (\Exception $exception){
                    echo $exception->getMessage();
                    DB::rollBack();
                }
            }


            usleep(1000*500);//500毫秒
        }
    }
}